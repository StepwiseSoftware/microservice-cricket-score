This is a .Net Core application which I built to see whether I could. I've been to some training courses led by Allen Holub, who advocates microservices as an architecture if you need to deploy small changes often.

I'm an experienced C# developer, but inexperienced in .Net Core, so there might be better ways of doing some things.

There is a project of base classes, MicroBase, with some unit tests.
Microservices might be best communicating either in-process on the same server, or communicating via REST, or communicating by messaging (such as ZeroMQ). There is a project, with some unit tests, for each Transport type.
There is an implementation of a Naming Service using REST, and I'll add later further implementations of other services using REST, then add a ZeroMQ implementation to see whether REST or ZeroMQ work better.

Friendly comments welcome.