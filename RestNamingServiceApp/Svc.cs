﻿using MicroBase.Core;
using MicroBase.CoreServices;
using RestMicroserviceTransport.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestNamingServiceApp
{
    public static class Svc
    {
        private static NamingService namingService = null;
        public static void Set(IMessageTransporter messageTransporter)
        {
            namingService = new NamingService("Initial naming service", messageTransporter);
        }
        public static NamingService Get()
        {
            return namingService;
        }
    }
}
