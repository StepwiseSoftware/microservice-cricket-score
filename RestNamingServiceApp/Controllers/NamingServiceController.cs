﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroBase.Core;
using MicroBase.CoreServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestMicroserviceTransport.Core;
using RestMicroserviceTransport.Http;

namespace RestNamingServiceApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NamingServiceController : ControllerBase
    {
        private RestJsonSerializer jsonSerializer = new RestJsonSerializer();
        private RestJsonDeserializer jsonDeserializer = new RestJsonDeserializer();

        [Route("health")]
        [HttpGet]
        public ActionResult<String> Health()
        {
            Svc.Get().StartListening();
            return Ok("UP");
        }

        [HttpGet]
        public ActionResult<IEnumerable<RestServiceLocation>> Get()
        {
            return GetServices(Svc.Get());
        }

        /// <summary>
        /// curl -i -X POST "http://localhost:5000/api/namingService/receiveRequest" -d "{\"payload\":{\"serviceType\":\"NamingService\",\"serviceLocation\":{\"serviceType\":\"NamingService\", \"url\":\"http://localhost:5000\"}}}" -H "Content-Type:application/json"
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("receiveRequest")]
        [HttpPost]
        public ActionResult ReceiveRequest(String request)
        {
            JRequest jRequest = Deserialize(request);
            Svc.Get().ReceiveRequest(jRequest);
            return Accepted();
        }

        /// <summary>
        /// curl -i -X POST "http://localhost:5000/api/namingService/respondToRequest" -d "{\"payload\":{\"serviceType\":\"NamingService\",\"serviceLocation\":{\"serviceType\":\"NamingService\", \"url\":\"http://localhost:5000\"}}}" -H "Content-Type:application/json"
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("respondToRequest")]
        [HttpPost]
        public ActionResult<RequestResponse> RespondToRequest(String request)
        {
            return Ok(Serialize(Svc.Get().RespondToRequest(Deserialize(request))));
        }

        /// <summary>
        /// curl -i -X POST "http://localhost:5000/api/namingService/start" -d "{\"payload\":{\"serviceType\":\"NamingService\",\"serviceLocation\":{\"serviceType\":\"NamingService\", \"url\":\"http://localhost:5000\"}}}" -H "Content-Type:application/json"
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("start")]
        [HttpPost]
        public ActionResult<RequestResponse> Start([FromBody] OnStartRequest request)
        {
            return Ok(Svc.Get().RespondToRequest(request).ToString());
        }

        /// <summary>
        /// curl -i -X POST "http://localhost:5000/api/namingService/stop" -d "{\"payload\":{\"serviceType\":\"NamingService\",\"serviceLocation\":{\"serviceType\":\"NamingService\", \"url\":\"http://localhost:5000\"}}}" -H "Content-Type:application/json"
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("stop")]
        [HttpPost]
        public ActionResult<RequestResponse> Stop([FromBody] OnStopRequest request)
        {
            return Ok(Svc.Get().RespondToRequest(request).ToString());
        }

        private JRequest Deserialize(string request)
        {
            return jsonDeserializer.DeserializeRequest(request);
        }

        private String Serialize(IRequestResponse requestResponse)
        {
            return jsonSerializer.SerializeObject(requestResponse);
        }

        //TODO: Move to a manager class, add tests 
        private List<RestServiceLocation> GetServices(NamingService namingService)
        {
            List<RestServiceLocation> list = new List<RestServiceLocation>();
            foreach (KnownService serviceType in Enum.GetValues(typeof(KnownService)))
            {
                list.AddRange(namingService.FindServicesByName(serviceType)
                    .Select(sl => sl as RestServiceLocation)
                    .Where(sl => sl != null));
            }
            return list;
        }
    }
}