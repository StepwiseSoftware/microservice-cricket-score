﻿using MicroBase.Core;
using System;
using System.Threading.Tasks;

namespace InProcessTransport.Core
{
    /// <summary>
    /// For proving the code all hangs together without using ZeroMQ or similar
    /// </summary>
    public class InProcessMessageTransporter : MessageTransporter
    {
        public override IServiceLocation GetServiceLocation(BaseMicroservice baseMicroservice)
        {
            return new InProcessServiceLocation(baseMicroservice);
        }

        public override void SendMessageToService(IRequest request)
        {
            InProcessServiceFactory.GetServiceOfType(request.ReceivingService, this).ReceiveRequest(request);
        }

        public override async Task<IRequestResponse> SendMessageToServiceWithReply(IRequest request)
        {
            return InProcessServiceFactory.GetServiceOfType(request.ReceivingService, this).RespondToRequest(request);
        }
    }


}
