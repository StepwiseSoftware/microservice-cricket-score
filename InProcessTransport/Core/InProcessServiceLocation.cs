﻿using MicroBase;
using MicroBase.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace InProcessTransport.Core
{
    /// <summary>
    /// The means by which a service is found when we want to call it
    /// </summary>
    //We have three classes inheriting from IServiceLocation, one for IP for ZeroMQ, one for DNS for REST, one for UnitTests
    public class InProcessServiceLocation : IServiceLocation
    {
        private readonly BaseMicroservice serviceImplementation;
        public InProcessServiceLocation(BaseMicroservice serviceImplementation)
        {
            this.serviceImplementation = serviceImplementation;
        }

        public override bool Equals(object obj)
        {
            var location = obj as InProcessServiceLocation;
            return location != null &&
                   EqualityComparer<BaseMicroservice>.Default.Equals(serviceImplementation, location.serviceImplementation);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(serviceImplementation);
        }

        public BaseMicroservice GetServiceImplementation() { return serviceImplementation; }

        public KnownService ServiceType
        {
            get {
                return this.serviceImplementation.GetServiceType();
            }
        }
    }
}
