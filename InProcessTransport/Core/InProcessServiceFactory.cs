﻿using MicroBase;
using MicroBase.Core;
using MicroBase.CoreServices;
using MicroBase.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace InProcessTransport.Core
{
    public class InProcessServiceFactory
    {
        private static NamingService namingService = null;
        public static BaseMicroservice GetServiceOfType(KnownService serviceName, IMessageTransporter messageTransporter)
        {
            if (serviceName.Equals(KnownService.NamingService))
            {
                return GetNamingService(messageTransporter);
            }

            var namingService = GetNamingService(messageTransporter);
            var list = namingService.FindServicesByName(serviceName);
            if (list.Count > 0)
            {
                return ((InProcessServiceLocation)list[0]).GetServiceImplementation();
            }

            if (serviceName.Equals(KnownService.WriteToFileService))
            {
                return new WriteToFileService("An empty service which just writes to the Console", messageTransporter);
            }
            throw new ArgumentException(String.Format("Unknown service {0}", serviceName));
        }

        private static NamingService GetNamingService(IMessageTransporter messageTransporter)
        {
            if (namingService == null)
            {
                namingService = new NamingService("An implementation using just an in-memory Dictionary", messageTransporter);
            }
            return namingService;
        }
    }
}
