﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroBase.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestMicroserviceTransport;
using RestMicroserviceTransport.Core;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private RestMessageTransporter messageTransporter;
        private BaseMicroservice service;

        ServiceController(IMessageTransporter messageTransporter)
        {
            this.messageTransporter = messageTransporter as RestMessageTransporter;
            if (service == null)
            {
                service = RestServiceHelper.GetServiceOfType(KnownService.NamingService, this.messageTransporter);
                service.StartListening();
            }
        }

        // POST: api/Service
        [HttpPost]
        public ActionResult<string> Post([FromBody] string messageBody)
        {
            //TODO: Error handling
            Request request = ParseMessageBody(messageBody, out bool waitForResponse);
            if (waitForResponse) {
                var response = messageTransporter.SendMessageToServiceWithReply(request);

                return SerializeResponse(response.Result);
            } else {
                messageTransporter.SendMessageToService(request);

                return new AcceptedResult();
            }
        }

        private ActionResult<string> SerializeResponse(IRequestResponse response)
        {
            //TODO: Make this work
            return Ok("It worked");
        }

        protected Request ParseMessageBody(string messageBody, out bool waitForResponse)
        {
            waitForResponse = true;
            //TODO: Make this work
            return new Request(KnownService.NamingService, new SuccessFailurePayload(true));
        }
    }
}
