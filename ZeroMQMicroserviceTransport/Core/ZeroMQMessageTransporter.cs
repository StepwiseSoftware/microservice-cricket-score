﻿using MicroBase;
using MicroBase.Core;
using System;
using System.Threading.Tasks;

namespace ZeroMQMicroserviceTransport.Core
{
    public class ZeroMQMessageTransporter : MessageTransporter
    {
        public override IServiceLocation GetServiceLocation(BaseMicroservice baseMicroservice)
        {
            throw new NotImplementedException();
        }

        public override void SendMessageToService(IRequest request)
        {
            throw new NotImplementedException();
        }

        public override async Task<IRequestResponse> SendMessageToServiceWithReply(IRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
