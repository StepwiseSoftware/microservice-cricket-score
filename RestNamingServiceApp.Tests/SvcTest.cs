using InProcessTransport.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RestNamingServiceApp.Tests
{
    [TestClass]
    public class SvcTest
    {
        [TestInitialize]
        public void Setup()
        {
            Svc.Set(new InProcessMessageTransporter());
        }


        [TestMethod]
        public void Get_shouldWork()
        {
            var service = Svc.Get();
            Assert.IsNotNull(service);
            var service2 = Svc.Get();
            Assert.AreEqual(service, service2);
        }
    }
}
