﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace RestMicroserviceTransport
{
    public static class RestProgramHelper
    {
        private static String publicUrl;
        public static void ConfigurePort(IWebHostBuilder builder, String[] args)
        {
            var portArg = args.ToList().FindAll(x => x.StartsWith("port=")).FirstOrDefault();
            if (portArg != null)
            {
                //TODO: Change this for times when we aren't all running on one box
                publicUrl = "http://localhost:" + portArg.Replace("port=", "");
                String localUrl = "http://localhost:" + portArg.Replace("port=", "");
                builder.UseUrls(localUrl);
            }
        }
        public static String GetCurrentUrl() { return publicUrl; }
    }
}
