﻿using MicroBase.Core;
using MicroBase.CoreServices;
using RestMicroserviceTransport.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestMicroserviceTransport.Http
{
    public class JRequest : Request
    {
        public JRequest(KnownService receivingService, String action, RequestId correlationId, IPayload payload) : base(receivingService, correlationId, payload)
        {
        }
    }
    public class NamingServiceJRequest : JRequest
    {
        public NamingServiceJRequest(String action, RequestId correlationId, IPayload payload) : base(KnownService.NamingService, action, correlationId, payload)
        {
        }
    }
    public class SuccessFailureRequest : NamingServiceJRequest
    {
        public SuccessFailureRequest(String action, RequestId correlationId, SuccessFailurePayload payload) : base(action, correlationId, payload)
        {
        }
    }
    public class OnStartRequest: NamingServiceJRequest
    {
        public OnStartRequest(RequestId correlationId, OnStartRestPayload payload) : base("Start", correlationId, payload)
        {
        }
    }
    public class OnStopRequest : NamingServiceJRequest
    {
        public OnStopRequest(RequestId correlationId, OnStopRestPayload payload) : base("Stop", correlationId, payload)
        {
        }
    }
    public class FindServiceOfTypeRequest : NamingServiceJRequest
    {
        public FindServiceOfTypeRequest(RequestId correlationId, FindServiceOfTypePayload payload) : base("FindServiceOfType", correlationId, payload)
        {
        }
    }
    public class OnStartRestPayload : OnStartPayload
    {
        public OnStartRestPayload(RestServiceLocation serviceLocation, String start) : base(serviceLocation)
        { }
    }
    public class OnStopRestPayload : OnStopPayload
    {
        public OnStopRestPayload(RestServiceLocation serviceLocation, String stop) : base(serviceLocation)
        { }
    }
}
