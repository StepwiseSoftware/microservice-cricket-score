﻿using MicroBase.Core;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RestMicroserviceTransport
{
    public interface IRestHttpHelper { }

    public class RestHttpHelper : IRestHttpHelper
    {
        private readonly IHttpClientFactory clientFactory;
        public RestHttpHelper(IHttpClientFactory clientFactory)
        {
            this.clientFactory = clientFactory;
        }
        internal async void SendMessage(string url, string postMessage)
        {
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, url);
            var client = clientFactory.CreateClient();
            var response = await client.SendAsync(httpRequestMessage);
            if (response.IsSuccessStatusCode)
            {
                //TODO: Log info
            }
            else
            {
                //TODO: Log error
            }
        }
        internal async Task<IRequestResponse> SendMessageWithReply(IRequest originalRequest, string url, string postMessage)
        {
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, url);
            var client = clientFactory.CreateClient();
            var response = await client.SendAsync(httpRequestMessage);
            //TODO: Log info
            var requestResponse = await response.Content.ReadAsAsync<IRequestResponse>();
            return requestResponse;
        }

        private IPayload GeneratePayload(string url, string postMessage, bool success, string responseContent)
        {
            return new SuccessFailurePayload(success);
        }
    }
}
