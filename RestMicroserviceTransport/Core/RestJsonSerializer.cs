﻿using System;
using System.IO;
using MicroBase.Core;
using MicroBase.CoreServices;
using Newtonsoft.Json;

namespace RestMicroserviceTransport.Core
{
    public class RestJsonSerializer
    {
        JsonSerializer serializer;
        public RestJsonSerializer()
        {
            serializer =  new JsonSerializer();
        }

        public string SerializeObject<T>(T input)
        {
            return JsonConvert.SerializeObject(input);
        }
    }
}