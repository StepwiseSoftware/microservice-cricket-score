﻿using MicroBase;
using MicroBase.Core;
using MicroBase.CoreServices;
using System;
using System.Threading.Tasks;

namespace RestMicroserviceTransport.Core
{
    public class RestMessageTransporter : MessageTransporter
    {
        private readonly RestJsonSerializer jsonSerializer;
        private readonly RestHttpHelper restHttpHelper; //TODO: Should be IRestHttpHelper
        public RestMessageTransporter(IRestHttpHelper restHttpHelper)
        {
            jsonSerializer = new RestJsonSerializer();
            this.restHttpHelper = (RestHttpHelper)restHttpHelper;
        }

        private readonly String namingServiceUrl = "http://localhost:5000";

        public override IServiceLocation GetServiceLocation(BaseMicroservice baseMicroservice)
        {
            return GetServiceLocation(baseMicroservice.GetServiceType());
        }

        private RestServiceLocation GetServiceLocation(KnownService serviceType)
        {
            if (serviceType.Equals(KnownService.NamingService))
            {
                return new RestServiceLocation(serviceType, namingServiceUrl);
            }
            else
            {
                //TODO: Fix this, call naming service
                return new RestServiceLocation(serviceType, namingServiceUrl);
            }
        }

        public override void SendMessageToService(IRequest request)
        {
            RestServiceLocation restServiceLocation = GetServiceLocation(request.ReceivingService);
            String url = restServiceLocation.Url + GetUrlSuffix(request, false);
            String postMessage = GetPostMessage(request);
            restHttpHelper.SendMessage(restServiceLocation.Url, postMessage);
        }

        private string GetPostMessage(IRequest request)
        {
            string message = "";
            if (request != null && request.Payload != null)
            {
                return jsonSerializer.SerializeObject(request.Payload);
            }
            return message;
        }

        public override async Task<IRequestResponse> SendMessageToServiceWithReply(IRequest request)
        {
            RestServiceLocation restServiceLocation = GetServiceLocation(request.ReceivingService);
            String url = restServiceLocation.Url + GetUrlSuffix(request, true);
            String postMessage = GetPostMessage(request);
            return await restHttpHelper.SendMessageWithReply(request, url, postMessage);
        }

        private string GetUrlSuffix(IRequest request, bool waitForResponse)
        {
            if (request != null && request.Payload != null)
            {
                if (request.Payload.GetType().Equals(typeof(OnStartPayload)))
                {
                    return "/start";
                }
                if (request.Payload.GetType().Equals(typeof(OnStopPayload)))
                {
                    return "/stop";
                }
                if (request.Payload.GetType().Equals(typeof(FindServiceOfTypePayload)))
                {
                    return "";
                }
                return waitForResponse ? "respondToRequest" : "receiveRequest";
            }
            return "";
        }
    }
}
