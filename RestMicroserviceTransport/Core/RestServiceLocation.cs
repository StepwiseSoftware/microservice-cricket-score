﻿using MicroBase.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestMicroserviceTransport.Core
{
    public class RestServiceLocation : IServiceLocation
    {
        public KnownService ServiceType { get; private set; }
        public String Url { get; private set; }
        public RestServiceLocation(KnownService serviceType, String url)
        {
            this.Url = url.EndsWith("/") ? url.Remove(url.Length-1) : url;
            this.ServiceType = serviceType;
        }
        public String GetServiceTypeAsString() { return Enum.GetName(typeof(KnownService), ServiceType); }

        public override bool Equals(object obj)
        {
            var location = obj as RestServiceLocation;
            return location != null &&
                   ServiceType == location.ServiceType &&
                   Url == location.Url;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ServiceType, Url);
        }
    }
}
