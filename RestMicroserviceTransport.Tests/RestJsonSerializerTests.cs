﻿using MicroBase.Core;
using MicroBase.CoreServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestMicroserviceTransport.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestMicroserviceTransport.Tests
{
    [TestClass]
    public class RestJsonSerializerTests
    {
        private readonly RestJsonSerializer serializer = new RestJsonSerializer();

        [TestMethod]
        public void SerializeRequestResponse_shouldGiveExpectedResults()
        {
            OnStartPayload inputPayload = new OnStartPayload(new RestServiceLocation(KnownService.NamingService, "http://localhost:5094"));
            SuccessFailurePayload outputPayload = new SuccessFailurePayload(true);
            Request originalRequest = new Request(KnownService.NamingService, inputPayload);
            RequestResponse response = new RequestResponse(originalRequest, outputPayload);
            String actualResults = serializer.SerializeObject(response);
            Assert.IsTrue(actualResults.Contains("\"OriginalRequest\":"));
            Assert.IsTrue(actualResults.Contains("\"ReceivingService\":0"));    //TODO: Correct the 0 to "NamingService"
            Assert.IsTrue(actualResults.Contains("\"Payload\":{\"ServiceLocation\":{\"ServiceType\":0,\"Url\":\"http://localhost:5094\"}}"));    //TODO: Correct the 0 to "NamingService"
            Assert.IsTrue(actualResults.Contains("\"Payload\":{\"Success\":true}"));
        }

        [TestMethod]
        public void SerializePayload_shouldWorkForSuccessFailureFalse()
        {
            String expectedResults = "{\"Success\":false}";
            SuccessFailurePayload inputPayload = new SuccessFailurePayload(false);
            Assert.AreEqual(expectedResults, serializer.SerializeObject(inputPayload));
        }

        [TestMethod]
        public void SerializePayload_shouldWorkForSuccessFailureTrue()
        {
            String expectedResults = "{\"Success\":true}";
            SuccessFailurePayload inputPayload = new SuccessFailurePayload(true);
            Assert.AreEqual(expectedResults, serializer.SerializeObject(inputPayload));
        }
    }
}
