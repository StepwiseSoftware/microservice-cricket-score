using MicroBase.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestMicroserviceTransport.Core;

namespace RestMicroserviceTransport.Tests
{
    [TestClass]
    public class RestServiceLocationTests
    {
        [TestMethod]
        public void Equality()
        {
            var restServiceLocation1 = new RestServiceLocation(KnownService.NamingService, "http://localhost:5000");
            var restServiceLocation2 = new RestServiceLocation(KnownService.NamingService, "http://localhost:5000");
            var restServiceLocation3 = new RestServiceLocation(KnownService.WriteToFileService, "http://localhost:5000");
            var restServiceLocation4 = new RestServiceLocation(KnownService.NamingService, "http://localhost:5001");
            var restServiceLocation5 = new RestServiceLocation(KnownService.NamingService, "https://localhost:5000");
            var restServiceLocation6 = new RestServiceLocation(KnownService.NamingService, "http://localhost:5000/");

            Assert.AreEqual(restServiceLocation1, restServiceLocation2);
            Assert.AreNotEqual(restServiceLocation1, restServiceLocation3);
            Assert.AreNotEqual(restServiceLocation1, restServiceLocation4);
            Assert.AreNotEqual(restServiceLocation1, restServiceLocation5);
            Assert.AreEqual(restServiceLocation1, restServiceLocation6);
        }
    }
}
