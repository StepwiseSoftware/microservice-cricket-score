﻿using System;
using System.Collections.Generic;
using InProcessTransport.Core;
using MicroBase.Core;
using MicroBase.CoreServices;

namespace MicroCricketScore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting Microservice infrastructure");

            IMessageTransporter messageTransporter = new InProcessMessageTransporter();
            NamingService namingService = InProcessServiceFactory.GetServiceOfType(KnownService.NamingService, messageTransporter) as NamingService;
            namingService.StartListening();

            //Now we have a message transporter for services to communicate, and a naming service, we can start other services
            InProcessServiceFactory.GetServiceOfType(KnownService.WriteToFileService, messageTransporter).StartListening();

            Console.WriteLine("Microservice infrastructure started. Listing services running:");
            Console.WriteLine("---Start---");

            ListRunningServices(namingService);

            Console.WriteLine("---End---");
            Console.ReadLine();
            Console.WriteLine("Shutting down Microservice infrastructure");

            ShutdownRunningServices(namingService);

            Console.WriteLine("Closing. Hit Return to end");
            Console.ReadLine();
        }

        private static void ShutdownRunningServices(NamingService namingService)
        {
            foreach (KnownService serviceType in Enum.GetValues(typeof(KnownService)))
            {
                var list = GetListOfRunningServices(namingService, serviceType);
                while (list.Count > 0)
                {
                    InProcessServiceLocation serviceLocation = (InProcessServiceLocation)list[0];
                    if (serviceLocation.GetServiceImplementation() != null)
                    {
                        serviceLocation.GetServiceImplementation().StopListening();
                    }
                    list = GetListOfRunningServices(namingService, serviceType);
                }
            }
        }

        private static void ListRunningServices(NamingService namingService)
        {
            foreach (KnownService serviceType in Enum.GetValues(typeof(KnownService)))
            {
                foreach (IServiceLocation serviceLocation in GetListOfRunningServices(namingService, serviceType))
                {
                    Console.WriteLine("   " + serviceType.ToString() + " at " + serviceLocation.ToString());
                }
            }
        }

        private static List<IServiceLocation> GetListOfRunningServices(NamingService namingService, KnownService serviceType)
        {
            ListPayload<IServiceLocation> listPayload = namingService.RespondToRequest(
                new Request(KnownService.NamingService, new FindServiceOfTypePayload(serviceType))).Payload as ListPayload<IServiceLocation>;
            if (listPayload != null)
            {
                return listPayload.GetList();
            }
            return new List<IServiceLocation>();
        }
    }
}
