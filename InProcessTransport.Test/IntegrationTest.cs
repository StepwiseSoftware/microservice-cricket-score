using InProcessTransport.Core;
using MicroBase.Core;
using MicroBase.CoreServices;
using MicroBase.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InProcessTransport.Test
{
    [TestClass]
    public class IntegrationTest
    {
        private IMessageTransporter GetMessageTransporter() { return new InProcessMessageTransporter(); }
        private void StartService(IMessageTransporter messageTransporter, KnownService knownService)
        {
            var service = InProcessServiceFactory.GetServiceOfType(knownService, messageTransporter);
            service.StartListening();
        }

        private void StopService(IMessageTransporter messageTransporter, KnownService knownService)
        {
            var service = InProcessServiceFactory.GetServiceOfType(knownService, messageTransporter);
            service.StopListening();
        }

        private ListPayload<IServiceLocation> GetServices(IMessageTransporter messageTransporter, KnownService serviceType)
        {
            var request = new Request(KnownService.NamingService, new FindServiceOfTypePayload(serviceType));
            var response = messageTransporter.SendMessageToServiceWithReply(request);
            response.Wait();
            return response.Result.Payload as ListPayload<IServiceLocation>;
        }

        [TestMethod]
        public void StartingAServiceStartsIt()
        {
            IMessageTransporter messageTransporter = GetMessageTransporter();
            StartService(messageTransporter, KnownService.NamingService);

            var list = GetServices(messageTransporter, KnownService.NamingService);
            Assert.AreEqual(1, list.GetList().Count);
            Assert.AreEqual(typeof(NamingService), ((InProcessServiceLocation)list.GetList()[0]).GetServiceImplementation().GetType());

            var list2 = GetServices(messageTransporter, KnownService.WriteToFileService);
            Assert.AreEqual(0, list2.GetList().Count);

            StartService(messageTransporter, KnownService.WriteToFileService);

            var list3 = GetServices(messageTransporter, KnownService.NamingService);
            Assert.AreEqual(1, list3.GetList().Count);
            Assert.AreEqual(typeof(NamingService), ((InProcessServiceLocation)list3.GetList()[0]).GetServiceImplementation().GetType());

            var list4 = GetServices(messageTransporter, KnownService.WriteToFileService);
            Assert.AreEqual(1, list4.GetList().Count);
            Assert.AreEqual(typeof(WriteToFileService), ((InProcessServiceLocation)list4.GetList()[0]).GetServiceImplementation().GetType());

            StopService(messageTransporter, KnownService.WriteToFileService);

            var list5 = GetServices(messageTransporter, KnownService.NamingService);
            Assert.AreEqual(1, list5.GetList().Count);
            Assert.AreEqual(typeof(NamingService), ((InProcessServiceLocation)list5.GetList()[0]).GetServiceImplementation().GetType());

            var list6 = GetServices(messageTransporter, KnownService.WriteToFileService);
            Assert.AreEqual(0, list6.GetList().Count);

            StopService(messageTransporter, KnownService.NamingService);
        }
    }
}
