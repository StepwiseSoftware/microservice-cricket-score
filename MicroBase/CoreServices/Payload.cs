﻿using MicroBase.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroBase.CoreServices
{
    /// <summary>
    /// Used to tell the naming service what services are running, when a service starts
    /// </summary>
    public class OnStartPayload : Payload
    {
        public IServiceLocation ServiceLocation;
        public OnStartPayload(IServiceLocation serviceLocation)
        {
            this.ServiceLocation = serviceLocation;
        }

        public override bool Equals(object obj)
        {
            var payload = obj as OnStartPayload;
            return payload != null &&
                   EqualityComparer<IServiceLocation>.Default.Equals(ServiceLocation, payload.ServiceLocation);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ServiceLocation);
        }
    }

    /// <summary>
    /// Used to tell the naming service that a service is no longer running, when a service stops
    /// </summary>
    public class OnStopPayload : Payload
    {
        private readonly IServiceLocation serviceLocation;
        public OnStopPayload(IServiceLocation serviceLocation)
        {
            this.serviceLocation = serviceLocation;
        }

        public override bool Equals(object obj)
        {
            var payload = obj as OnStopPayload;
            return payload != null &&
                   EqualityComparer<IServiceLocation>.Default.Equals(serviceLocation, payload.serviceLocation);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(serviceLocation);
        }

        public IServiceLocation GetServiceLocation() { return serviceLocation; }
    }
    
    /// <summary>
    /// Used to find a service of a particular type
    /// </summary>
    public class FindServiceOfTypePayload : Payload
    {
        private readonly KnownService serviceType;
        public FindServiceOfTypePayload(KnownService serviceType)
        {
            this.serviceType = serviceType;
        }

        public override bool Equals(object obj)
        {
            var payload = obj as FindServiceOfTypePayload;
            return payload != null &&
                   serviceType == payload.serviceType;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(serviceType);
        }

        public KnownService GetServiceType() { return serviceType; }
    }
}
