﻿using MicroBase.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroBase.CoreServices
{
    //TODO: Consider internal
    public class NamingService : BaseMicroservice
    {
        private Dictionary<KnownService, List<IServiceLocation>> runningServices;

        public NamingService(String microserviceDescription, IMessageTransporter messageTransporter) : base(KnownService.NamingService, messageTransporter, microserviceDescription) {
            runningServices = new Dictionary<KnownService, List<IServiceLocation>>();
        }

        //Idempotent
        private void AddRunningService(IServiceLocation serviceLocation)
        {
            if (runningServices.ContainsKey(serviceLocation.ServiceType)) {
                if (!runningServices[serviceLocation.ServiceType].Contains(serviceLocation)) {
                    runningServices[serviceLocation.ServiceType].Add(serviceLocation);
                }
            } else
            {
                runningServices.Add(serviceLocation.ServiceType, new List<IServiceLocation> { serviceLocation });
            }
        }
        //Idempotent
        private bool RemoveRunningService(IServiceLocation serviceLocation)
        {
            bool isRemoved = false;
            if (runningServices.ContainsKey(serviceLocation.ServiceType))
            {
                if (runningServices[serviceLocation.ServiceType].Contains(serviceLocation))
                {
                    runningServices[serviceLocation.ServiceType].Remove(serviceLocation);
                    isRemoved = true;
                }
                if (runningServices[serviceLocation.ServiceType].Count == 0)
                {
                    runningServices.Remove(serviceLocation.ServiceType);
                }
            }
            return isRemoved;
        }
        public List<IServiceLocation> FindServicesByName(KnownService serviceType)
        {
            if (runningServices.ContainsKey(serviceType))
            {
                return runningServices[serviceType];
            }
            else
            {
                return new List<IServiceLocation>();
            }
        }
        protected override void OnRequestReceived(IRequest request)
        {
            if (!request.ReceivingService.Equals(KnownService.NamingService))
                throw new ArgumentException("Request wrongly sent to NamingService: " + request.ToString());
            IPayload payload = request.Payload;
            OnStartPayload onStartPayload = payload as OnStartPayload;
            if (onStartPayload != null)
            {
                AddRunningService(onStartPayload.ServiceLocation);
                return;
            }
            OnStopPayload onStopPayload = payload as OnStopPayload;
            if (onStopPayload != null)
            {
                RemoveRunningService(onStartPayload.ServiceLocation);
                return;
            }
            throw new ArgumentException("Invalid payload");
        }

        protected override IRequestResponse HandleRequestWithReply(IRequest request)
        {
            if (!request.ReceivingService.Equals(KnownService.NamingService))
                throw new ArgumentException("Request wrongly sent to NamingService: " + request.ToString());
            IPayload payload = request.Payload;
            OnStartPayload onStartPayload = payload as OnStartPayload;
            if (onStartPayload != null)
            {
                AddRunningService(onStartPayload.ServiceLocation);
                return new RequestResponse(request, new SuccessFailurePayload(true));
            }
            OnStopPayload onStopPayload = payload as OnStopPayload;
            if (onStopPayload != null)
            {
                bool result = RemoveRunningService(onStopPayload.GetServiceLocation());
                return new RequestResponse(request, new SuccessFailurePayload(result));
            }
            FindServiceOfTypePayload findServiceOfTypePayload = payload as FindServiceOfTypePayload;
            if (findServiceOfTypePayload != null)
            {
                List<IServiceLocation> list = FindServicesByName(findServiceOfTypePayload.GetServiceType());
                return new RequestResponse(request, new ListPayload<IServiceLocation>(list));
            }
            throw new ArgumentException("Invalid payload");
        }
    }
}
