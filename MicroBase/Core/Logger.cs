﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroBase.Core
{
    enum Severity : int
    {
        Debug,Info,Warn,Error
    }

    class Logger
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Logger() { }

        public void LogInfo(String message) {
            log.Info(message);
        }

        public void LogError(String message)
        {
            log.Error(message);
        }

        public void LogError(String message, Exception exception)
        {
            log.Error(message, exception);
        }
    }
}
