﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroBase.Core
{
    public interface IRequestResponse
    {
        IPayload Payload { get; }
        IRequest OriginalRequest { get; }
    }

    /// <summary>
    /// The response from a request
    /// </summary>
    public class RequestResponse : IRequestResponse
    {
        public IRequest OriginalRequest { get; private set; }
        public RequestId ResponseId { get; private set; }
        public RequestId CorrelationId { get; private set; } //The same for all similar requests as they are passed through the system
        public IPayload Payload { get; private set; }  //The data which is the response

        public RequestResponse(IRequest originalRequest, IPayload payload)
        {
            this.OriginalRequest = originalRequest;
            this.ResponseId = new RequestId();
            this.CorrelationId = originalRequest.CorrelationId;
            this.Payload = payload;
        }

        public override bool Equals(object obj)
        {
            var response = obj as RequestResponse;
            return response != null &&
                   EqualityComparer<IRequest>.Default.Equals(OriginalRequest, response.OriginalRequest) &&
                   EqualityComparer<RequestId>.Default.Equals(ResponseId, response.ResponseId) &&
                   EqualityComparer<RequestId>.Default.Equals(CorrelationId, response.CorrelationId) &&
                   EqualityComparer<IPayload>.Default.Equals(Payload, response.Payload);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(OriginalRequest, ResponseId, CorrelationId, Payload);
        }

        public override string ToString()
        {
            return String.Format("Response payload: {0}; original request: {1}", OriginalRequest, Payload);
        }
    }
}
