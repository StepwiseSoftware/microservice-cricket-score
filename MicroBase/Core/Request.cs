﻿using System;
using System.Collections.Generic;

namespace MicroBase.Core
{
    /// <summary>
    /// An id for a request, or response
    /// </summary>
    public class RequestId
    {
        public String InternalId { get; private set; }

        public RequestId()
        {
            InternalId = Guid.NewGuid().ToString();
        }

        public RequestId(String newId)
        {
            InternalId = newId;
        }

        public override bool Equals(object obj)
        {
            var id = obj as RequestId;
            return id != null &&
                   InternalId == id.InternalId;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(InternalId);
        }

        public override string ToString()
        {
            return InternalId;
        }

        //Probably not needed
        //String getId() { return internalId; }
    }

    public interface IRequest {
        IPayload Payload { get; }
        RequestId CorrelationId { get; }
        KnownService ReceivingService { get; }
    }

    /// <summary>
    /// A request being made to a service
    /// </summary>
    public class Request : IRequest
    {
        public RequestId Id { get; private set; }
        public RequestId CorrelationId { get; private set; }  //The same for all similar requests as they are passed through the system
        public KnownService ReceivingService { get; private set; } //The service which needs to fulfil the request
        public IPayload Payload { get; private set; } //The data required to complete the request

        public Request(KnownService receivingService, IPayload payload) : this(receivingService, new RequestId(), payload) { }
        public Request(KnownService receivingService, RequestId correlationId, IPayload payload)
        {
            this.Id = new RequestId();
            this.CorrelationId = correlationId;
            this.Payload = payload;
            this.ReceivingService = receivingService;
        }

        public override string ToString()
        {
            return String.Format("Request id: {0}; correlation id: {1}; receivingService: {2}; payload: {3}", Id, CorrelationId, ReceivingService, Payload);
        }

        public override bool Equals(object obj)
        {
            var request = obj as Request;
            return request != null &&
                   EqualityComparer<RequestId>.Default.Equals(Id, request.Id) &&
                   EqualityComparer<RequestId>.Default.Equals(CorrelationId, request.CorrelationId) &&
                   ReceivingService == request.ReceivingService &&
                   EqualityComparer<IPayload>.Default.Equals(Payload, request.Payload);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, CorrelationId, ReceivingService, Payload);
        }

        //Probably not needed
        //RequestId getId() { return id;  }
    }

    public class JsonRequest : Request { JsonRequest(KnownService receivingService, IPayload payload) : base(receivingService, payload) { } }

    public class XmlRequest : Request { XmlRequest(KnownService receivingService, IPayload payload) : base(receivingService, payload) { } }
}
