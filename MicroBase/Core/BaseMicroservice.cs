﻿using MicroBase.CoreServices;
using System;
using System.Threading.Tasks;

namespace MicroBase.Core
{
    public interface IMicroservice {
        void StartListening();
        void StopListening();
        void ReceiveRequest(IRequest request);
        IRequestResponse RespondToRequest(IRequest request);
    }

    public abstract class BaseMicroservice : IMicroservice
    {
        private readonly Logger logger;
        private readonly KnownService serviceType;
        private readonly String microserviceClassName;
        private readonly String microserviceDescription;
        private readonly String serverIdentifier;
        private readonly String classInstanceIdentifier;    //So that we can see messages sent from this instance of this class, for logging 
        private readonly IMessageTransporter messageTransporter;
        private IServiceLocation serviceLocation = null;
        private bool isListening;

        public BaseMicroservice(KnownService serviceType, IMessageTransporter messageTransporter, String microserviceDescription)
        {
            this.logger = new Logger();
            this.serviceType = serviceType;
            this.messageTransporter = messageTransporter;
            this.microserviceClassName = this.GetType().Name;   //Better would be to return the function it fulfils
            this.microserviceDescription = microserviceDescription ?? "";   //TODO: Change this to be option<String>
            serverIdentifier = GetServerIdentifier();
            classInstanceIdentifier = Guid.NewGuid().ToString();
        }

        protected virtual void OnStartListening() { }  //To be overridden in a base class, if required
        protected virtual void OnStopListening() { }  //To be overridden in a base class, if required
        protected abstract void OnRequestReceived(IRequest request);   //Must be overridden in a base class
        protected abstract IRequestResponse HandleRequestWithReply(IRequest request);   //Must be overridden in a base class
        protected virtual void DoSecurityCheck(IRequest request) { }   //May be implemented or not in a base class

        public void StartListening() {
            logger.LogInfo(String.Format("Microservice {0} starting to listen for requests on server {1}, class instance {2}",
                microserviceClassName, serverIdentifier, classInstanceIdentifier));
            if (!isListening)
            {
                var namingRequestReply = messageTransporter.SendMessageToServiceWithReply(
                    new Request(KnownService.NamingService, new OnStartPayload(this.GetCurrentServiceLocation())));
                namingRequestReply.Wait();
                if ((namingRequestReply.Result.Payload as SuccessFailurePayload).Success)
                {
                    isListening = true;
                    OnStartListening();
                }
                else
                {
                    throw new Exception("Exception in StartListening");
                }
            }
            logger.LogInfo(String.Format("Microservice {0} started to listen for requests on server {1}, class instance {2}",
                microserviceClassName, serverIdentifier, classInstanceIdentifier));
        }

        public void ReceiveRequest(IRequest request)
        {
            logger.LogInfo(String.Format("Microservice {0} on class instance {1} received request {2}",
                microserviceClassName, classInstanceIdentifier, request.ToString()));
            DoSecurityCheck(request);
            Task.Run(() => OnRequestReceived(request));
        }

        public IRequestResponse RespondToRequest(IRequest request)
        {
            logger.LogInfo(String.Format("Microservice {0} on class instance {1} received request needing reply {2}",
                microserviceClassName, classInstanceIdentifier, request.ToString()));
            DoSecurityCheck(request);
            IRequestResponse requestReply = HandleRequestWithReply(request);
            logger.LogInfo(String.Format("Microservice {0} on class instance {1} replying to request {2} with response {3}",
                microserviceClassName, classInstanceIdentifier, request.ToString(), requestReply.ToString()));
            return requestReply;
        }

        public void StopListening() {
            logger.LogInfo(String.Format("Microservice {0} stopping listening on server {1}, class instance {2}",
                microserviceClassName, serverIdentifier, classInstanceIdentifier));
            if (isListening)
            {
                var namingEndRequestReply = messageTransporter.SendMessageToServiceWithReply(
                    new Request(KnownService.NamingService, new OnStopPayload(GetCurrentServiceLocation())));
                namingEndRequestReply.Wait();
                if ((namingEndRequestReply.Result.Payload as SuccessFailurePayload).Success)
                {
                    isListening = false;
                    OnStopListening();
                }
                else
                {
                    throw new Exception("Exception in StartListening");
                }
            }
            logger.LogInfo(String.Format("Microservice {0} stopped listening on server {1}, class instance {2}",
                microserviceClassName, serverIdentifier, classInstanceIdentifier));
        }

        /// <summary>
        /// Return the machine name of the server running this code, for logging purposes
        /// </summary>
        /// <returns></returns>
        protected String GetServerIdentifier()
        {
            return Environment.MachineName;
        }

        //Return the current service location of this service
        public IServiceLocation GetCurrentServiceLocation()
        {
            if (serviceLocation == null)
            {
                serviceLocation = FindMyCurrentServiceLocation();
            }
            return serviceLocation;
        }

        private IServiceLocation FindMyCurrentServiceLocation()
        {
            return messageTransporter.GetServiceLocation(this);
        }

        public KnownService GetServiceType()
        {
            return serviceType;
        }
    }
}
