﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroBase.Core
{
    public interface IServiceLocation {
        KnownService ServiceType { get; }
    }
}
