﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroBase.Core
{
    /// <summary>
    /// All requests and responses have a payload, which will be serialized as a string in JSON or XML
    /// </summary>
    public interface IPayload {
        //        IPayload FromJson(String json);
        //        IPayload FromXml(String xml);
        //        String ToJson();
        //        String ToXml();
    }

    /// <summary>
    /// Base class for concrete instances, which handles serialization/deserialization common logic
    /// </summary>
    public abstract class Payload : IPayload {
        //        public abstract String ToJson();
        //        public abstract String ToXml();
    }

    /// <summary>
    /// A deserialization class for each payload type. Not sure of the best design for this
    /// </summary>
    public static class PayloadDeserializer<T> where T : IPayload {
        public static T FromJson(String json) { return default(T); }
        public static T FromXml(String xml) { return default(T); }
    }

    public class SuccessFailurePayload: Payload {

        public bool Success { get; private set; }

        public SuccessFailurePayload(bool success) {
            this.Success = success;
        }

        public override bool Equals(object obj)
        {
            var payload = obj as SuccessFailurePayload;
            return payload != null &&
                   Success == payload.Success;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Success);
        }

        public override string ToString()
        {
            return String.Format("SuccessFailurePayload success:{0}", Success);
        }
    }

    public class ListPayload<T> : Payload
    {
        private readonly List<T> list;
        public ListPayload(List<T> list)
        {
            this.list = list;
        }

        public override bool Equals(object obj)
        {
            var payload = obj as ListPayload<T>;
            return payload != null &&
                   EqualityComparer<List<T>>.Default.Equals(list, payload.list);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(list);
        }

        public List<T> GetList() { return this.list; }
    }
}
