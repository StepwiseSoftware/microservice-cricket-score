﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MicroBase.Core
{
    public interface IMessageTransporter {
        void SendMessageToService(IRequest request);
        Task<IRequestResponse> SendMessageToServiceWithReply(IRequest request);
        IServiceLocation GetServiceLocation(BaseMicroservice baseMicroservice);
    }

    public abstract class MessageTransporter : IMessageTransporter
    {
        public abstract void SendMessageToService(IRequest request);
        public abstract Task<IRequestResponse> SendMessageToServiceWithReply(IRequest request);
        public abstract IServiceLocation GetServiceLocation(BaseMicroservice baseMicroservice);
    }
}
