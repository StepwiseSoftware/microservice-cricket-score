﻿using MicroBase.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroBase.Services
{
    public class WriteToFilePayload : Payload
    {
        private readonly String path;
        private readonly String text;
        private readonly bool append;
        private readonly bool addLineBreak;
        public WriteToFilePayload(String path, String text, bool append, bool addLineBreak)
        {
            this.path = path;
            this.text = text;
            this.append = append;
            this.addLineBreak = addLineBreak;
        }
        public String GetPath() { return path; }
        public String GetText() { return text; }
        public bool GetAppend() { return append; }
        public bool GetAddLineBreak() { return addLineBreak; }
    }
}
