﻿using MicroBase;
using MicroBase.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroBase.Services
{
    public class WriteToFileService : BaseMicroservice
    {
        private FileHandler fileHandler;
        public WriteToFileService(string microserviceDescription, IMessageTransporter messageTransporter) : base(KnownService.WriteToFileService, messageTransporter, microserviceDescription)
        {
            fileHandler = new FileHandler();
        }

        protected override IRequestResponse HandleRequestWithReply(IRequest request)
        {
            WriteToFilePayload payload = request.Payload as WriteToFilePayload;
            if (payload != null)
            {
                HandlePayload(payload);
                return new RequestResponse(request, new SuccessFailurePayload(true));
            }
            throw new ArgumentException("Invalid payload");
        }

        private void HandlePayload(WriteToFilePayload payload)
        {
            if (payload.GetAppend())
            {
                if (payload.GetAddLineBreak())
                {
                    fileHandler.AppendLineToFile(payload.GetPath(), payload.GetText());
                }
                else
                {
                    fileHandler.AppendToFile(payload.GetPath(), payload.GetText());
                }
            }
            else
            {
                if (payload.GetAddLineBreak())
                {
                    fileHandler.WriteLineToFile(payload.GetPath(), payload.GetText());
                }
                else
                {
                    fileHandler.WriteToFile(payload.GetPath(), payload.GetText());
                }
            }
        }

        protected override void OnRequestReceived(IRequest request)
        {
            WriteToFilePayload payload = request.Payload as WriteToFilePayload;
            if (payload != null)
            {
                HandlePayload(payload);
            }
            throw new ArgumentException("Invalid payload");
        }
    }
}
