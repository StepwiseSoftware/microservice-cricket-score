using InProcessTransport.Core;
using MicroBase.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Threading;

namespace MicroBase.Test
{
    [TestClass]
    public class BaseMicroserviceTest
    {
        [TestMethod]
        public void BaseMicroservice_ShouldExecuteAndReturnSynchronously()
        {
            Stopwatch stopwatch = new Stopwatch();
            var messageTransporter = new InProcessMessageTransporter();
            var sleepService = new SleepService(messageTransporter);
            sleepService.StartListening();

            stopwatch.Start();
            sleepService.RespondToRequest(new Request(KnownService.SleepService, new SuccessFailurePayload(true)));
            stopwatch.Stop();

            sleepService.StopListening();

            Assert.IsTrue(stopwatch.ElapsedMilliseconds > 999);
        }
        [TestMethod]
        public void BaseMicroservice_ShouldExecuteAsynchronously()
        {
            Stopwatch stopwatch = new Stopwatch();
            var messageTransporter = new InProcessMessageTransporter();
            var sleepService = new SleepService(messageTransporter);
            sleepService.StartListening();

            stopwatch.Start();
            sleepService.ReceiveRequest(new Request(KnownService.SleepService, new SuccessFailurePayload(true)));
            stopwatch.Stop();

            sleepService.StopListening();

            Assert.IsTrue(stopwatch.ElapsedMilliseconds < 1000);
        }
    }

    class SleepService : BaseMicroservice
    {
        private readonly int duration = 1000;

        public SleepService(IMessageTransporter messageTransporter) :
            base(KnownService.SleepService, messageTransporter, "Sleeps for 1 second, for unit test purposes")
        { }

        protected override IRequestResponse HandleRequestWithReply(IRequest request)
        {
            Sleep(duration);
            return new RequestResponse(request, new SuccessFailurePayload(true));
        }

        protected override void OnRequestReceived(IRequest request)
        {
            Sleep(duration);
        }

        private void Sleep(int milliseconds)
        {
            Thread.Sleep(milliseconds);
        }
    }
}
