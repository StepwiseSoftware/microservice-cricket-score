﻿using InProcessTransport.Core;
using MicroBase.Core;
using MicroBase.CoreServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroBase.Test
{
    [TestClass]
    public class NamingServiceTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NamingService_shouldRejectInvalidRequests()
        {
            var namingService = new NamingService("UnitTest", new InProcessMessageTransporter());
            namingService.RespondToRequest(new Request(KnownService.WriteToFileService, 
                new OnStartPayload(new InProcessServiceLocation(namingService))));
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NamingService_shouldRejectInvalidPayloads()
        {
            var namingService = new NamingService("UnitTest", new InProcessMessageTransporter());
            namingService.RespondToRequest(new Request(KnownService.NamingService,
                new SuccessFailurePayload(true)));
        }
        [TestMethod]
        public void NamingService_shouldAcceptValidStartMessage_NamingService()
        {
            var namingService = new NamingService("UnitTest", new InProcessMessageTransporter());
            var request = new Request(KnownService.NamingService,
                new OnStartPayload(new InProcessServiceLocation(namingService)));
            var response = namingService.RespondToRequest(request);
            Assert.AreNotEqual(new RequestResponse(request, new SuccessFailurePayload(true)), response);
            Assert.AreEqual(new SuccessFailurePayload(true), response.Payload);
            Assert.AreEqual(request, response.OriginalRequest);
        }
        [TestMethod]
        public void NamingService_shouldAcceptValidStartMessage_Twice()
        {
            var namingService = new NamingService("UnitTest", new InProcessMessageTransporter());
            var request = new Request(KnownService.NamingService,
                new OnStartPayload(new InProcessServiceLocation(namingService)));
            var response1 = namingService.RespondToRequest(request);
            var response2 = namingService.RespondToRequest(request);
            Assert.AreEqual(response1.Payload, response2.Payload);
        }
        [TestMethod]
        public void NamingService_shouldAcceptValidStartMessage_TwoRequests()
        {
            var namingService = new NamingService("UnitTest", new InProcessMessageTransporter());
            var request1 = new Request(KnownService.NamingService,
                new OnStartPayload(new InProcessServiceLocation(namingService)));
            var request2 = new Request(KnownService.NamingService,
                new OnStartPayload(new InProcessServiceLocation(namingService)));
            var response1 = namingService.RespondToRequest(request1);
            var response2 = namingService.RespondToRequest(request2);
            Assert.AreEqual(response1.Payload, response2.Payload);
        }

        [TestMethod]
        public void NamingService_shouldAcceptValidStopMessage_NamingService()
        {
            var namingService = new NamingService("UnitTest", new InProcessMessageTransporter());
            var request = new Request(KnownService.NamingService,
                new OnStopPayload(new InProcessServiceLocation(namingService)));
            var response = namingService.RespondToRequest(request);
            Assert.AreEqual(new SuccessFailurePayload(false), response.Payload);
            Assert.AreEqual(request, response.OriginalRequest);
        }
        [TestMethod]
        public void NamingService_shouldCountServicesCorrectly()
        {
            var namingService = new NamingService("UnitTest", new InProcessMessageTransporter());
            var request = new Request(KnownService.NamingService,
                new OnStopPayload(new InProcessServiceLocation(namingService)));
            var response = namingService.RespondToRequest(request);
            Assert.AreEqual(new SuccessFailurePayload(false), response.Payload);

            var response2 = namingService.RespondToRequest(new Request(KnownService.NamingService,
                new FindServiceOfTypePayload(KnownService.NamingService)));
            Assert.AreEqual(typeof(ListPayload<IServiceLocation>), response2.Payload.GetType());
            Assert.AreEqual(0, (response2.Payload as ListPayload<IServiceLocation>).GetList().Count);

            var response3 = namingService.RespondToRequest(new Request(KnownService.NamingService,
                new OnStartPayload(new InProcessServiceLocation(namingService))));
            Assert.AreEqual(new SuccessFailurePayload(true), response3.Payload);

            var response4 = namingService.RespondToRequest(new Request(KnownService.NamingService,
                new FindServiceOfTypePayload(KnownService.NamingService)));
            Assert.AreEqual(1, (response4.Payload as ListPayload<IServiceLocation>).GetList().Count);
            Assert.AreEqual(new InProcessServiceLocation(namingService),
                (response4.Payload as ListPayload<IServiceLocation>).GetList()[0]);

            var response5 = namingService.RespondToRequest(new Request(KnownService.NamingService,
                new OnStartPayload(new InProcessServiceLocation(namingService))));
            Assert.AreEqual(new SuccessFailurePayload(true), response5.Payload);

            var response6 = namingService.RespondToRequest(new Request(KnownService.NamingService,
                new FindServiceOfTypePayload(KnownService.NamingService)));
            Assert.AreEqual(1, (response6.Payload as ListPayload<IServiceLocation>).GetList().Count);
            Assert.AreEqual(new InProcessServiceLocation(namingService),
                (response6.Payload as ListPayload<IServiceLocation>).GetList()[0]);

            var response7 = namingService.RespondToRequest(new Request(KnownService.NamingService,
                new OnStopPayload(new InProcessServiceLocation(namingService))));
            Assert.AreEqual(new SuccessFailurePayload(true), response7.Payload);

            var response8 = namingService.RespondToRequest(new Request(KnownService.NamingService,
                new FindServiceOfTypePayload(KnownService.NamingService)));
            Assert.AreEqual(0, (response8.Payload as ListPayload<IServiceLocation>).GetList().Count);

        }
    }
}
